<?php

namespace App\Http\Controllers;

use App\Mail\SendOrder;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class MainController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function feedback(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'email' => 'sometimes|nullable|string|max:100|email',
            'phone' => 'required|max:20',
            'service' => 'required|filled',
            'message' => 'required|max:500'
        ]);

        $order = new Order;

        $order->name = $request->name;
        $order->email = $request->filled('email') ? $request->email : null;
        $order->phone = $request->phone;
        $order->service = $request->service;
        $order->text = $request->message;

        $order->save();

        Mail::to('vitality.yablochkin@gmail.com')
            ->send(new SendOrder($order));

        $response = 'Ваше сообщение успешно отправлено!';

        return response()->json(array('msg'=> $response), 200);
    }

}
