<?php

use App\Order;

Route::get('/', 'MainController@index');

Route::post('/', 'MainController@feedback');

Route::get('/mail', function () {
    return new \App\Mail\SendOrder(Order::find(1));
});