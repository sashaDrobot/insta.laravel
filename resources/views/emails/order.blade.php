@component('mail::message')
    # Новое сообщение

    Было отправлено новое сообщение!

    Контактные данные:

    Имя: {{ $data->name }}
    Email: {{ $data->email }}
    Телефон: {{ $data->phone }}
    Услуга: {{ $data->service }}
    Сообщение: {{ $data->text }}

@endcomponent